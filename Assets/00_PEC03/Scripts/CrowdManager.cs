using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CrowdManager : MonoBehaviour
{
    // Minimum wait time before an agent moves to the next target
    public float minWaitTime = 1f;

    // Maximum wait time before an agent moves to the next target
    public float maxWaitTime = 5f;

    // Minimum speed of the agents
    public float minSpeed = 1.2f;

    // Maximum speed of the agents
    public float maxSpeed = 2f;

    // Distance an agent should run away from a zombie
    public float runAwayDistance = 5f;

    // Safe distance from a zombie to return to normal state
    public float safeDistance = 10f;

    // Time after which an agent will despawn
    public float DespawnTime = 10f;

    // List of crowd agents
    private List<GameObject> crowdAgents;

    // List of crowd targets
    private List<Transform> crowdTargets;

    // Dictionary to track if a target is assigned
    private Dictionary<Transform, bool> targetAssigned;

    void Start()
    {
        // Initialize the lists and dictionary
        crowdAgents = new List<GameObject>(GameObject.FindGameObjectsWithTag("CrowdAgent"));
        crowdTargets = new List<Transform>();
        targetAssigned = new Dictionary<Transform, bool>();

        // Populate the target list and dictionary
        foreach (GameObject target in GameObject.FindGameObjectsWithTag("CrowdTarget"))
        {
            crowdTargets.Add(target.transform);
            targetAssigned.Add(target.transform, false);
        }

        // Start moving each agent
        foreach (GameObject agent in crowdAgents)
        {
            StartCoroutine(MoveAgent(agent));
        }
    }

    // Coroutine to handle the movement of an agent
    public IEnumerator MoveAgent(GameObject agent)
    {
        NavMeshAgent navMeshAgent = agent.GetComponent<NavMeshAgent>();
        Animator animator = agent.GetComponent<Animator>();
        CitizenState citizenState = agent.GetComponent<CitizenState>();

        // Perform null checks for essential components
        if (navMeshAgent == null)
        {
            yield break; // Exit coroutine early if NavMeshAgent is missing
        }

        if (animator == null)
        {
            yield break; // Exit coroutine early if Animator is missing
        }

        if (citizenState == null)
        {
            yield break; // Exit coroutine early if CitizenState is missing
        }

        while (true)
        {
            // Check if NavMeshAgent is still valid
            if (navMeshAgent == null || !navMeshAgent.isOnNavMesh)
            {
                yield break; // Exit coroutine if NavMeshAgent is null or not on NavMesh
            }

            switch (citizenState.currentState)
            {
                case CitizenState.State.Normal:
                    // Get a random target and set the agent's destination
                    Transform randomTarget = GetRandomTarget();
                    if (randomTarget != null && navMeshAgent != null)
                    {
                        navMeshAgent.SetDestination(randomTarget.position);
                        targetAssigned[randomTarget] = true;

                        // Set the agent's animation mode to walking
                        animator.SetInteger("Mode", 1);

                        // Wait until the agent reaches the destination
                        while (navMeshAgent.isOnNavMesh && (navMeshAgent.pathPending || navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance))
                        {
                            yield return null;
                        }

                        // Set the agent's animation mode to idle
                        animator.SetInteger("Mode", 0);

                        // Wait for a random time before moving to the next target
                        float waitTime = Random.Range(minWaitTime, maxWaitTime);
                        yield return new WaitForSeconds(waitTime);

                        // Mark the target as unassigned
                        targetAssigned[randomTarget] = false;
                    }
                    break;

                case CitizenState.State.Alerted:
                    // Handle the agent running away from the nearest zombie
                    while (citizenState.currentState == CitizenState.State.Alerted)
                    {
                        Transform nearestZombie = citizenState.GetNearestZombie();
                        if (nearestZombie != null && navMeshAgent != null)
                        {
                            Vector3 directionAwayFromZombie = (agent.transform.position - nearestZombie.position).normalized;
                            Vector3 runToPosition = agent.transform.position + directionAwayFromZombie * runAwayDistance;

                            navMeshAgent.SetDestination(runToPosition);

                            // If the agent is far enough from the zombie, return to normal state
                            if (Vector3.Distance(agent.transform.position, nearestZombie.position) > safeDistance)
                            {
                                citizenState.SetState(CitizenState.State.Normal);
                                yield break;
                            }
                        }
                        yield return null;
                    }
                    break;

                case CitizenState.State.Knocked:
                    // Remove the agent from the crowdAgents list if knocked
                    crowdAgents.Remove(agent);
                    break;
            }

            yield return null;
        }
    }

    // Get a random target from the available targets
    Transform GetRandomTarget()
    {
        List<Transform> availableTargets = new List<Transform>();

        // Add unassigned targets to the available targets list
        foreach (Transform target in crowdTargets)
        {
            if (!targetAssigned[target])
            {
                availableTargets.Add(target);
            }
        }

        // Return a random target if available, otherwise return null
        if (availableTargets.Count > 0)
        {
            return availableTargets[Random.Range(0, availableTargets.Count)];
        }
        else
        {
            return null;
        }
    }
}
