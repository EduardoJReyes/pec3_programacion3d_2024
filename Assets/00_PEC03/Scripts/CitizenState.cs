using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CitizenState : MonoBehaviour
{
    // Enumeration for the different states of the citizen
    public enum State
    {
        Normal,
        Alerted,
        Knocked
    }

    // Current state of the citizen
    public State currentState = State.Normal;

    // NavMeshAgent component for navigating the citizen
    private NavMeshAgent navMeshAgent;

    // Animator component for controlling animations
    private Animator animator;

    // Distance at which the citizen becomes alerted by a zombie
    public float alertDistance = 2f;

    // Duration the citizen stays alerted before returning to normal
    public float alertDuration = 2f;

    // Reference to the nearest zombie
    private Transform nearestZombie;

    // Coroutine for handling the alert state duration
    private Coroutine alertCoroutine;

    // Audio clips for different screams
    public AudioClip scream1;
    public AudioClip scream2;

    void Start()
    {
        // Set initial state to normal
        currentState = State.Normal;

        // Get the NavMeshAgent and Animator components
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        // Check for nearby zombies
        DetectZombies();

        // Handle behavior based on current state
        switch (currentState)
        {
            case State.Normal:
                break;
            case State.Alerted:
                break;
            case State.Knocked:
                break;
        }
    }

    // Detect nearby zombies and update state accordingly
    private void DetectZombies()
    {
        if (currentState == State.Knocked) return;

        GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");
        nearestZombie = null;
        float nearestDistance = alertDistance;

        foreach (GameObject zombie in zombies)
        {
            float distance = Vector3.Distance(transform.position, zombie.transform.position);
            if (distance < nearestDistance)
            {
                nearestDistance = distance;
                nearestZombie = zombie.transform;
            }
        }

        if (nearestZombie != null)
        {
            SetState(State.Alerted);
        }
        else if (currentState == State.Alerted && alertCoroutine == null)
        {
            alertCoroutine = StartCoroutine(DelayedNormalState());
        }
    }

    // Set the state of the citizen
    public void SetState(State newState)
    {
        if (newState == State.Alerted && currentState != State.Alerted)
        {
            currentState = newState;
            if (alertCoroutine != null)
            {
                StopCoroutine(alertCoroutine);
                alertCoroutine = null;
            }

            // Update NavMeshAgent speed and animation for alerted state
            navMeshAgent.speed = 2f;
            animator.SetInteger("Mode", 2);
            PlaySoundAtPosition(scream1);
        }
        else
        {
            currentState = newState;

            switch (currentState)
            {
                case State.Normal:
                    // Update NavMeshAgent speed and animation for normal state
                    navMeshAgent.speed = 2f;
                    if (navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
                    {
                        animator.SetInteger("Mode", 1);
                    }
                    else
                    {
                        animator.SetInteger("Mode", 0);
                    }
                    break;
                case State.Knocked:
                    // Handle actions when the citizen is knocked
                    PlaySoundAtPosition(scream2);

                    // Find and destroy the MinimapIcon child GameObject
                    Transform minimapIconTransform = transform.Find("MinimapIcon");
                    if (minimapIconTransform != null)
                    {
                        Destroy(minimapIconTransform.gameObject);
                    }

                    navMeshAgent.enabled = false;
                    animator.enabled = false;
                    GetComponent<CapsuleCollider>().enabled = false;
                    enabled = false;
                    break;
            }
        }
    }

    // Coroutine to delay returning to normal state after being alerted
    private IEnumerator DelayedNormalState()
    {
        yield return new WaitForSeconds(alertDuration);
        if (currentState == State.Alerted)
        {
            SetState(State.Normal);
        }
        alertCoroutine = null;
    }

    // Handle collision with other objects
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Car"))
        {
            animator.enabled = false;
            navMeshAgent.enabled = false;
            GetComponent<CapsuleCollider>().enabled = false;
        }
    }

    // Handle trigger entry with other colliders
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            SetState(State.Knocked);
        }
        else if (other.CompareTag("Zombie"))
        {
            SetState(State.Knocked);
        }
        else if (other.CompareTag("Car"))
        {
            SetState(State.Knocked);
        }
    }

    // Get the nearest zombie detected by the citizen
    public Transform GetNearestZombie()
    {
        return nearestZombie;
    }

    // Method to play a sound at the citizen's position
    private void PlaySoundAtPosition(AudioClip clip)
    {
        GameObject audioObject = new GameObject("TempAudio");
        audioObject.transform.position = transform.position;
        AudioSource tempAudioSource = audioObject.AddComponent<AudioSource>();
        tempAudioSource.clip = clip;
        tempAudioSource.spatialBlend = 1.0f; // Make the sound 3D
        tempAudioSource.Play();
        Destroy(audioObject, clip.length); // Destroy the audio object after the clip has finished playing
    }
}
