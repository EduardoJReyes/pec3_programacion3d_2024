using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;  // Ensure you have this to use NavMeshAgent

public class ZombieActivator : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        // Check if the "K" key is pressed
        if (Input.GetKeyDown(KeyCode.K))
        {
            // Find all objects tagged as "Zombie"
            GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");

            // Loop through each zombie and activate its components
            foreach (GameObject zombie in zombies)
            {
                // Activate the Animator
                Animator animator = zombie.GetComponent<Animator>();
                if (animator != null)
                {
                    animator.enabled = true;
                }

                // Activate the NavMeshAgent
                NavMeshAgent navMeshAgent = zombie.GetComponent<NavMeshAgent>();
                if (navMeshAgent != null)
                {
                    navMeshAgent.enabled = true;
                }

                // Activate the ZombieScript (assuming it's a component)
                ZombieScript zombieScript = zombie.GetComponent<ZombieScript>();
                if (zombieScript != null)
                {
                    zombieScript.enabled = true;
                }

                // Activate the Capsule Collider
                CapsuleCollider capsuleCollider = zombie.GetComponent<CapsuleCollider>();
                if (capsuleCollider != null)
                {
                    capsuleCollider.enabled = true;
                }
            }
        }
    }
}
