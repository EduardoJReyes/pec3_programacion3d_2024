using UnityEngine;

public class FloatingObject : MonoBehaviour
{
    // Speed at which the object floats up and down
    [SerializeField] private float floatSpeed = 1f;

    // Range of the floating movement
    [SerializeField] private float floatRange = 0.5f;

    // Speed at which the object rotates
    [SerializeField] private float rotateSpeed = 30f;

    // Axis around which the object rotates
    [SerializeField] private Vector3 rotationAxis = Vector3.up;

    // Starting position of the object
    private Vector3 startPos;

    private void Start()
    {
        // Initialize the starting position
        startPos = transform.position;
    }

    private void Update()
    {
        // Floating movement
        Vector3 newPosition = startPos + Vector3.up * Mathf.Sin(Time.time * floatSpeed) * floatRange;
        transform.position = newPosition;

        // Rotation
        transform.Rotate(rotationAxis, rotateSpeed * Time.deltaTime);
    }
}
