using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using StarterAssets;
using UnityEngine.InputSystem;
using UnityEngine.Animations.Rigging;
using TMPro;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

public class ThirdPersonShooterController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera PlayerAimCamera; // Cinemachine virtual camera for aiming mode
    [SerializeField] private CinemachineVirtualCamera mainCamera; // Main Cinemachine virtual camera
    [SerializeField] private float normalSensitivity; // Normal mouse sensitivity
    [SerializeField] private float aimSensitivity; // Mouse sensitivity when aiming
    [SerializeField] private LayerMask aimColliderLayerMask = new LayerMask(); // Layer mask for aim raycast
    [SerializeField] private Transform debugTransform; // Debug transform for visualizing aim raycast
    [SerializeField] private Transform pfBulletProjectile; // Prefab for bullet projectile
    [SerializeField] private Transform spawnBulletPosition; // Spawn position for bullets
    [SerializeField] private GameObject playerModel; // Player model GameObject
    [SerializeField] private Transform PlayerCameraRoot; // Root transform of the player camera

    [SerializeField] private Transform pfBulletParticle; // Prefab for bullet particle effect
    [SerializeField] private Transform HurtParticle; // Particle effect for when player gets hurt

    [SerializeField] private RigBuilder rigBuilder; // RigBuilder component for rigging operations
    [SerializeField] private Transform aimTarget; // Target transform for aiming

    [SerializeField] private CanvasGroup bloodyOverlay; // Canvas group for bloody overlay effect

    [SerializeField] private Volume postProcessVolume; // Post-processing volume
    private ColorAdjustments colorAdjustments; // Reference to ColorAdjustments effect in post-processing volume

    [SerializeField] private TextMeshProUGUI bulletText; // UI text for displaying bullet count

    [SerializeField] private TrailRenderer trail1; // Trail renderer 1
    [SerializeField] private TrailRenderer trail2; // Trail renderer 2
    [SerializeField] private GameObject objectToHide; // GameObject to hide when entering a car

    private ThirdPersonController thirdPersonController; // Reference to ThirdPersonController component
    private StarterAssetsInputs starterAssetsInputs; // Reference to StarterAssetsInputs component
    private Animator animator; // Reference to Animator component
    private CarController currentCarController; // Reference to current CarController component
    private CarPatrol currentCarPatrol; // Reference to current CarPatrol component
    private bool isInCar = false; // Flag indicating if the player is inside a car
    private Transform carCameraRoot; // Root transform of the car's camera

    private int maxHealth = 100; // Maximum health of the player
    private int currentHealth; // Current health of the player

    private int maxBullets = 50; // Maximum number of bullets the player can carry
    private int currentBullets = 25; // Current number of bullets the player has

    private AudioSource audioSource; // Audio source component
    private GameObject SFXObject; // Reference to the GameObject holding SFX sounds
    public AudioClip shootSound; // Sound clip for shooting
    public AudioClip hurtSound; // Sound clip for getting hurt
    public AudioClip healSound; // Sound clip for healing
    public AudioClip deathSound; // Sound clip for player death
    public AudioClip ammoSound; // Sound clip for collecting ammo

    public LevelFade levelFade; // Reference to LevelFade script for fading transitions

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        SFXObject = GameObject.Find("SFX"); // Find SFX GameObject in the scene
        audioSource = SFXObject.GetComponent<AudioSource>(); // Get AudioSource component from SFX GameObject
    }

    private void Awake()
    {
        thirdPersonController = GetComponent<ThirdPersonController>(); // Get ThirdPersonController component
        starterAssetsInputs = GetComponent<StarterAssetsInputs>(); // Get StarterAssetsInputs component
        animator = GetComponent<Animator>(); // Get Animator component
        currentHealth = maxHealth; // Initialize current health to maximum health
        UpdateBloodyOverlay(); // Initialize the bloody overlay visibility

        if (postProcessVolume.profile.TryGet<ColorAdjustments>(out var colorAdjustments))
        {
            this.colorAdjustments = colorAdjustments; // Get ColorAdjustments effect from post-processing volume
        }

        UpdateBulletText(); // Initialize the bullet text
    }

    private void Update()
    {
        Vector3 mouseWorldPosition = Vector3.zero; // Initialize mouse world position

        Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f); // Calculate screen center point
        Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint); // Create ray from screen center

        if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, aimColliderLayerMask))
        {
            debugTransform.position = raycastHit.point; // Update debug transform position
            mouseWorldPosition = raycastHit.point; // Update mouse world position
        }

        if (starterAssetsInputs.aim)
        {
            PlayerAimCamera.gameObject.SetActive(true); // Activate aim camera
            thirdPersonController.SetSensitivity(aimSensitivity); // Set aiming sensitivity
            thirdPersonController.SetRotateOnMove(false); // Disable rotation on move

            Vector3 worldAimTarget = mouseWorldPosition;
            worldAimTarget.y = transform.position.y;
            Vector3 aimDirection = (worldAimTarget - transform.position).normalized;

            transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime * 20f); // Smoothly rotate towards aim direction

            aimTarget.position = mouseWorldPosition; // Set aim target position
        }
        else
        {
            PlayerAimCamera.gameObject.SetActive(false); // Deactivate aim camera
            thirdPersonController.SetSensitivity(normalSensitivity); // Set normal sensitivity
            thirdPersonController.SetRotateOnMove(true); // Enable rotation on move

            aimTarget.localPosition = Vector3.zero; // Reset aim target position
        }

        if (!isInCar && starterAssetsInputs.shoot && currentBullets > 0)
        {
            audioSource.PlayOneShot(shootSound); // Play shooting sound

            Transform bulletParticle = Instantiate(pfBulletParticle, spawnBulletPosition.position + Vector3.down/2, spawnBulletPosition.rotation); // Instantiate bullet particle
            bulletParticle.parent = spawnBulletPosition; // Set bullet particle parent

            Vector3 aimDir = (mouseWorldPosition - spawnBulletPosition.position).normalized; // Calculate aim direction
            Instantiate(pfBulletProjectile, spawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up)); // Instantiate bullet projectile

            starterAssetsInputs.shoot = false; // Reset shoot input

            currentBullets--; // Decrease current bullets count
            UpdateBulletText(); // Update bullet text
        }

        if (starterAssetsInputs.sidechange)
        {
            var newSide = PlayerAimCamera.GetCinemachineComponent<Cinemachine3rdPersonFollow>(); // Get Cinemachine3rdPersonFollow component
            newSide.CameraSide = 1f; // Set camera side
            starterAssetsInputs.sidechange = false; // Reset sidechange input
        }

        if (starterAssetsInputs.use)
        {
            if (!isInCar)
            {
                if (Physics.Raycast(ray, out RaycastHit hit, 7.5f))
                {
                    if (hit.transform.CompareTag("Car"))
                    {
                        EnterCar(hit.transform.GetComponent<CarController>(), hit.transform.GetComponent<CarPatrol>()); // Enter the car
                    }
                }

            }
            else
            {
                ExitCar(); // Exit the car
                starterAssetsInputs.use = false; // Reset use input
            }

            starterAssetsInputs.use = false; // Reset use input (fallback)
        }
        else
        {
            starterAssetsInputs.use = false; // Reset use input
        }
    }

    private void EnterCar(CarController carController, CarPatrol carPatrol)
    {
        currentCarPatrol = carPatrol; // Set current car patrol
        currentCarPatrol.enabled = false; // Disable car patrol
        currentCarController = carController; // Set current car controller
        if (currentCarController != null)
        {
            currentCarController.enabled = true; // Enable car controller
            isInCar = true; // Set isInCar flag to true
            thirdPersonController.enabled = false; // Disable third person controller
            playerModel.SetActive(false); // Hide player model
            trail1.gameObject.SetActive(false); // Hide trail 1
            trail2.gameObject.SetActive(false); // Hide trail 2
            objectToHide.SetActive(false); // Hide specified object

            transform.SetParent(currentCarController.transform); // Set player as child of the car

            carCameraRoot = currentCarController.transform.Find("CarCameraRoot"); // Find CarCameraRoot within the car
            if (carCameraRoot == null)
            {
                Debug.LogError("CarCameraRoot not found in the car."); // Log error if CarCameraRoot not found
                return;
            }

            Transform exitPosition = currentCarController.transform.Find("ExitPosition"); // Find ExitPosition within the car
            if (exitPosition != null)
            {
                transform.position = exitPosition.position; // Set player position to ExitPosition
                transform.rotation = exitPosition.rotation; // Set player rotation to ExitPosition rotation
            }
            else
            {
                Debug.LogError("ExitPosition not found in the car. Exiting car at default position.");
                transform.position = currentCarController.transform.position + currentCarController.transform.right * 2; // Default exit position
            }

            mainCamera.Follow = carCameraRoot; // Set main camera            mainCamera.Follow = carCameraRoot; // Set main camera to follow the car
            mainCamera.LookAt = carCameraRoot; // Set main camera to look at the car
        }
    }

    private void ExitCar()
    {
        if (currentCarController != null)
        {
            currentCarController.enabled = false; // Disable current car controller
            isInCar = false; // Set isInCar flag to false
            thirdPersonController.enabled = true; // Enable third person controller
            playerModel.SetActive(true); // Show player model
            trail1.gameObject.SetActive(true); // Show trail 1
            trail2.gameObject.SetActive(true); // Show trail 2
            objectToHide.SetActive(true); // Show specified object

            transform.SetParent(null); // Detach player from the car
            transform.position = currentCarController.transform.position + currentCarController.transform.right * 2; // Position player outside the car
            currentCarController = null; // Reset current car controller

            mainCamera.Follow = PlayerCameraRoot; // Set main camera to follow player camera root
            mainCamera.LookAt = PlayerCameraRoot; // Set main camera to look at player camera root
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Zombiehand>())
        {
            TakeDamage(10); // Take damage if collided with Zombiehand
        }
        else if (other.gameObject.CompareTag("Car"))
        {
            if (!isInCar)
            {
                TakeDamage(20); // Take damage if collided with a car and not inside one
            }
        }
        else if (other.gameObject.CompareTag("Medkit"))
        {
            Heal(20); // Heal when colliding with a medkit
            Destroy(other.gameObject); // Destroy the medkit object
        }
        else if (other.gameObject.CompareTag("Ammo"))
        {
            PlaySoundWithRandomPitch(ammoSound); // Play ammo collection sound

            currentBullets += 11; // Add bullets
            if (currentBullets > maxBullets)
            {
                currentBullets = maxBullets; // Cap bullets at maxBullets
            }
            UpdateBulletText(); // Update bullet text
            Destroy(other.gameObject); // Destroy the ammo object
        }
    }

    private void TakeDamage(int damage)
    {
        PlaySoundWithRandomPitch(hurtSound); // Play hurt sound

        Transform hurtParticle = Instantiate(HurtParticle, transform.position + Vector3.up * 1.5f, transform.rotation); // Instantiate hurt particle
        hurtParticle.parent = transform; // Set hurt particle parent

        currentHealth -= damage; // Decrease health
        UpdateBloodyOverlay(); // Update bloody overlay
        if (currentHealth <= 0)
        {
            Die(); // Die if health drops to or below zero
        }
    }

    private void Heal(int amount)
    {
        PlaySoundWithRandomPitch(healSound); // Play heal sound
        currentHealth += amount; // Increase health
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth; // Cap health at maxHealth
        }
        UpdateBloodyOverlay(); // Update bloody overlay
    }

    private void UpdateBloodyOverlay()
    {
        float alpha = 1 - ((float)currentHealth / maxHealth); // Calculate alpha for bloody overlay
        bloodyOverlay.alpha = alpha; // Set bloody overlay alpha

        float saturation = -100f * (1 - ((float)currentHealth / maxHealth)); // Calculate saturation for color adjustments
        if (colorAdjustments != null)
        {
            colorAdjustments.saturation.value = saturation; // Apply saturation to color adjustments
        }
    }

    private void UpdateBulletText()
    {
        bulletText.text = $"{currentBullets}/{maxBullets}"; // Update bullet count UI text
    }

    private void Die()
    {
        PlaySoundWithRandomPitch(deathSound); // Play death sound

        GetComponent<CharacterController>().enabled = false; // Disable CharacterController
        GetComponent<ThirdPersonController>().enabled = false; // Disable ThirdPersonController
        GetComponent<BasicRigidBodyPush>().enabled = false; // Disable BasicRigidBodyPush
        GetComponent<StarterAssetsInputs>().enabled = false; // Disable StarterAssetsInputs
        GetComponent<PlayerInput>().enabled = false; // Disable PlayerInput
        GetComponent<RigBuilder>().enabled = false; // Disable RigBuilder
        GetComponent<BoneRenderer>().enabled = false; // Disable BoneRenderer
        GetComponent<Animator>().enabled = false; // Disable Animator
        StartCoroutine(GameOver()); // Start game over coroutine

    }

    IEnumerator GameOver()
    {
        Cursor.lockState = CursorLockMode.None;
        yield return new WaitForSeconds(2.0f); 
        StartCoroutine(levelFade.FadeIn(0.25f)); // Start fade in using levelFade
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("02_GameOver"); // Load game over scene
        GetComponent<ThirdPersonShooterController>().enabled = false; // Disable ThirdPersonShooterController
    }

    //Pitch randomizer
    private void PlaySoundWithRandomPitch(AudioClip clip)
    {
        if (clip != null)
        {
            audioSource.pitch = Random.Range(0.8f, 1.2f); // Randomize pitch between 0.8 and 1.2
            audioSource.PlayOneShot(clip); // Play the sound clip
        }
    }
}
