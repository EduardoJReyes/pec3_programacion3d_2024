﻿using UnityEngine;

public class CarPatrol : MonoBehaviour
{
    // Speed at which the car moves
    public float MovementSpeed = 6f;

    // Speed at which the car turns
    public float TurningSpeed = 1f;

    // Tag used to identify waypoints
    public string WaypointTag;

    // Time to wait at each waypoint
    public float waypointCountdown = 5f;

    // Indicates if the car is currently moving
    bool IsMoving;

    // Distance to the next waypoint
    Vector3 distanceToNextWaypoint;

    // Indicates if the current waypoint has been reached
    bool waypointReached;

    // Starting point for the patrol
    GameObject StartingPoint;

    // Name of the target waypoint to go to
    string TargetWpToGo;

    // Current waypoint number in the sequence
    int CurrentWpNumber;

    // Rigidbody component for physics interactions
    Rigidbody rb;

    void Start()
    {
        // Start patrol after a delay
        Invoke("PatrolNow", 2);
        IsMoving = true;
    }

    // Initialize patrol settings and find the starting waypoint
    public void PatrolNow()
    {
        rb = GetComponent<Rigidbody>();
        StartingPoint = FindClosestWaypoint();
        TargetWpToGo = StartingPoint.gameObject.name;
        CurrentWpNumber = int.Parse(TargetWpToGo.Split(char.Parse("-"))[1]);
        IsMoving = true;
    }

    void Update()
    {
        // Move towards the current waypoint if the car is moving
        if (IsMoving)
        {
            GameObject WpToGo = GameObject.Find(WaypointTag + "-" + CurrentWpNumber);
            Vector3 lookPos = WpToGo.transform.position - transform.position;
            lookPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * TurningSpeed);
            transform.position += transform.forward * Time.deltaTime * MovementSpeed;
        }
    }

    // Find the closest waypoint to the car's current position
    public GameObject FindClosestWaypoint()
    {
        GameObject[] gos = GameObject.FindGameObjectsWithTag(WaypointTag);
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;

        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }

        return closest;
    }

    // Handle entering a waypoint trigger
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == WaypointTag && !waypointReached)
        {
            waypointReached = true;

            if (GameObject.Find(WaypointTag + "-" + (CurrentWpNumber + 1)) != null)
                CurrentWpNumber += 1;
            else
                CurrentWpNumber = 0;
        }
    }

    // Handle exiting a waypoint trigger
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == WaypointTag && waypointReached)
        {
            waypointReached = false;
        }
    }
}
