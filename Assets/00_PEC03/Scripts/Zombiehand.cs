using UnityEngine;

public class Zombiehand : MonoBehaviour
{
    private AudioSource audioSource; // Reference to AudioSource component
    public AudioClip conversionSound; // Sound clip for zombie conversion

    [SerializeField] private GameObject SFXObject; // Reference to the SFX object in the scene
    private ZombieScript zombieScript; // Reference to the ZombieScript component
    [SerializeField] private Transform ZombieExplosion; // Explosion effect for zombie conversion

    void Start()
    {
        SFXObject = GameObject.Find("SFX"); // Find and assign SFX object
        audioSource = SFXObject.GetComponent<AudioSource>(); // Get AudioSource component from SFX object

        // Get the ZombieScript component from the parent GameObject
        zombieScript = GetComponentInParent<ZombieScript>();
    }

    void OnTriggerEnter(Collider other)
    {
        // Check if zombieScript is assigned and in Attack state, and the collider is a CrowdAgent
        if (zombieScript != null && zombieScript.currentState == ZombieScript.ZombieState.Attack && other.CompareTag("CrowdAgent"))
        {
            // Create a new zombie at the position of the CrowdAgent
            Instantiate(zombieScript.zombiePrefab, other.transform.position, other.transform.rotation);

            // Play conversion sound
            audioSource.PlayOneShot(conversionSound);

            // Instantiate ZombieExplosion effect
            Instantiate(ZombieExplosion, transform.position, transform.rotation);

            // Destroy the CrowdAgent
            Destroy(other.gameObject);

            // Return to patrol state
            zombieScript.currentState = ZombieScript.ZombieState.Patrol;
            zombieScript.handCollider.enabled = false; // Disable hand collider after attack
            zombieScript.navMeshAgent.speed = zombieScript.patrolSpeed; // Set speed back to patrol speed
            zombieScript.SetRandomDestination(); // Set new random destination for patrolling
            zombieScript.SetAnimatorMode(1); // Set animator mode to walk (patrolling)
            zombieScript.SetAnimationSpeed(0.5f); // Set animation speed for patrolling
        }
    }
}
