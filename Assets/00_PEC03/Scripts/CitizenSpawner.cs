using System.Collections;
using UnityEngine;

public class CitizenSpawner : MonoBehaviour
{
    // Array of citizen prefabs to spawn
    public GameObject[] citizenPrefabs;

    // Time interval between spawns
    public float spawnInterval = 5f;

    // Maximum number of citizens that can be spawned
    public int maxCitizens = 100;

    // Array of spawn points for the citizens
    public Transform[] spawnPoints;

    // Current count of spawned citizens
    private int currentCitizenCount = 0;

    void Start()
    {
        // Start the coroutine to spawn citizens at intervals
        StartCoroutine(SpawnCitizensRoutine());
    }

    // Coroutine to handle the spawning of citizens
    IEnumerator SpawnCitizensRoutine()
    {
        while (true)
        {
            if (currentCitizenCount < maxCitizens)
            {
                // Select a random citizen prefab and spawn point
                GameObject citizenPrefab = citizenPrefabs[Random.Range(0, citizenPrefabs.Length)];
                Transform spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];

                // Instantiate the citizen at the spawn point
                GameObject spawnedCitizen = Instantiate(citizenPrefab, spawnPoint.position, Quaternion.identity);

                // Find the CrowdManager and start the citizen's movement coroutine
                CrowdManager crowdManager = FindObjectOfType<CrowdManager>();
                crowdManager.StartCoroutine(crowdManager.MoveAgent(spawnedCitizen));

                // Increment the current citizen count
                currentCitizenCount++;
            }
            // Wait for the specified spawn interval before spawning the next citizen
            yield return new WaitForSeconds(spawnInterval);
        }
    }

    // Set a random destination for the NavMesh agent
    void SetRandomDestination(UnityEngine.AI.NavMeshAgent agent)
    {
        Vector3 randomPoint = RandomNavSphere(agent.transform.position, 20f, -1);
        agent.SetDestination(randomPoint);
    }

    // Generate a random point within a specified distance from the origin
    Vector3 RandomNavSphere(Vector3 origin, float distance, int layerMask)
    {
        Vector3 randomDirection = Random.insideUnitSphere * distance;
        randomDirection += origin;

        UnityEngine.AI.NavMeshHit navHit;
        UnityEngine.AI.NavMesh.SamplePosition(randomDirection, out navHit, distance, layerMask);

        return navHit.position;
    }

    // Decrement the current citizen count
    public void DecrementCitizenCount()
    {
        currentCitizenCount--;
    }
}
