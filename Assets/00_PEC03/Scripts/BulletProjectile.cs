using UnityEngine;

public class BulletProjectile : MonoBehaviour
{
    // Variables to hold references to visual effects for different scenarios
    [SerializeField] private Transform vfxHitGreen;
    [SerializeField] private Transform vfxHit;
    [SerializeField] private Transform vfxCitizenDead;
    [SerializeField] private Transform vfxBlood;

    // Reference to the Rigidbody component of the bullet
    private Rigidbody bulletRigidbody;

    private void Awake()
    {
        // Get the Rigidbody component attached to the bullet
        bulletRigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        // Set the speed of the bullet and apply it to the Rigidbody's velocity
        float speed = 40f;
        bulletRigidbody.velocity = transform.forward * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        // Check what type of object the bullet collided with and instantiate appropriate visual effects
        if (other.GetComponent<CitizenState>())
        {
            Instantiate(vfxHit, transform.position, Quaternion.identity);
            CitizenState citizenState = other.GetComponent<CitizenState>();
            Instantiate(vfxCitizenDead, other.transform.position, Quaternion.identity);
        }
        else if (other.GetComponent<ZombieScript>())
        {
            Instantiate(vfxBlood, transform.position, Quaternion.identity);
            ZombieScript zombieScript = other.GetComponent<ZombieScript>();
            StartCoroutine(zombieScript.Death());
        }
        else
        {
            Instantiate(vfxHit, transform.position, Quaternion.identity);
        }

        // Destroy the bullet game object after hitting something
        Destroy(gameObject);
    }
}
