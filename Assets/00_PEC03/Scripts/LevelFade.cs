using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelFade : MonoBehaviour
{
    // Reference to the fade object for transition effects
    public GameObject fade;

    // Start the fade-out effect when the level starts
    private void Start()
    {
        StartCoroutine(FadeOut(0.5f));
    }

    // Coroutine to handle the fade-out effect
    public IEnumerator FadeOut(float duration)
    {
        Vector3 startPos = fade.transform.localPosition;
        Vector3 endPos = new Vector3(-1545, startPos.y, startPos.z);
        float elapsedTime = 0;

        // Gradually move the fade object to the end position over the given duration
        while (elapsedTime < duration)
        {
            fade.transform.localPosition = Vector3.Lerp(startPos, endPos, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        // Ensure the fade object is at the end position after the transition
        fade.transform.localPosition = endPos;
    }

    // Coroutine to handle the fade-in effect
    public IEnumerator FadeIn(float duration)
    {
        Vector3 startPos = fade.transform.localPosition;
        Vector3 endPos = new Vector3(0, startPos.y, startPos.z);
        float elapsedTime = 0;

        // Gradually move the fade object to the start position over the given duration
        while (elapsedTime < duration)
        {
            fade.transform.localPosition = Vector3.Lerp(startPos, endPos, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        // Ensure the fade object is at the start position after the transition
        fade.transform.localPosition = endPos;
    }
}
