using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    // UI elements
    public TMP_Dropdown graphicsDropdown;
    public Slider masterVol, musicVol, sfxVol;

    // Audio mixer to control audio levels
    public AudioMixer mainAudioMixer;

    // UI panels/groups
    public GameObject defaultGroup;
    public GameObject settingsGroup;

    // Fade object for transition effects
    public GameObject fade;

    // Switch UI to settings group
    public void ChangeToSettings()
    {
        defaultGroup.SetActive(false);
        settingsGroup.SetActive(true);
    }

    // Switch UI to default group
    public void ChangeToDefault()
    {
        defaultGroup.SetActive(true);
        settingsGroup.SetActive(false);
    }

    // Change graphics quality based on dropdown selection
    public void ChangeGraphicsQuality()
    {
        QualitySettings.SetQualityLevel(graphicsDropdown.value);
    }

    // Adjust master volume using slider
    public void ChangeMasterVolume()
    {
        mainAudioMixer.SetFloat("MasterVolume", masterVol.value);
    }

    // Adjust music volume using slider
    public void ChangeMusicVolume()
    {
        mainAudioMixer.SetFloat("MusicVolume", musicVol.value);
    }

    // Adjust SFX volume using slider
    public void ChangeSFXVolume()
    {
        mainAudioMixer.SetFloat("SFXVolume", sfxVol.value);
    }

    // Start the game, fading in with a specified duration
    public void StartTheGame()
    {
        StartCoroutine(FadeIn(0.25f));
    }

    // Coroutine for fading out with a specified duration
    public IEnumerator FadeOut(float duration)
    {
        Vector3 startPos = fade.transform.localPosition;
        Vector3 endPos = new Vector3(-1545, startPos.y, startPos.z);
        float elapsedTime = 0;

        // Gradually move the fade object to the end position over the given duration
        while (elapsedTime < duration)
        {
            fade.transform.localPosition = Vector3.Lerp(startPos, endPos, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        // Ensure the fade object is at the end position after the transition
        fade.transform.localPosition = endPos;
    }

    // Coroutine for fading in with a specified duration
    public IEnumerator FadeIn(float duration)
    {
        Vector3 startPos = fade.transform.localPosition;
        Vector3 endPos = new Vector3(0, startPos.y, startPos.z);
        float elapsedTime = 0;

        // Gradually move the fade object to the start position over the given duration
        while (elapsedTime < duration)
        {
            fade.transform.localPosition = Vector3.Lerp(startPos, endPos, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        // Ensure the fade object is at the start position after the transition
        fade.transform.localPosition = endPos;

        // Load the specified scene (in this case, "01_Level")
        SceneManager.LoadScene("01_Level");
    }

    // Start fading out when the menu manager starts
    void Start()
    {
        StartCoroutine(FadeOut(0.75f));
    }
}
