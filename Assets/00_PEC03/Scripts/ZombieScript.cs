using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class ZombieScript : MonoBehaviour
{
    public enum ZombieState { Patrol, Alerted, Pursue, Attack, Death }

    public ZombieState currentState; // Current state of the zombie
    public float detectionRadius = 10f; // Radius for detecting the player or other agents
    public float attackRadius = 2f; // Radius within which the zombie attacks
    public float patrolRadius = 20f; // Radius for patrolling around a point
    public float alertDuration = 0.5f; // Duration to remain alerted before pursuing
    public float attackCooldown = 1.5f; // Cooldown period after an attack
    public GameObject zombiePrefab; // Zombie prefab for potential spawning
    public Collider handCollider; // Collider attached to the zombie's hand

    public float pursueSpeed = 3.5f; // Speed when pursuing the target
    public float patrolSpeed = 1.75f; // Speed when patrolling

    private AudioSource audioSource; // Audio source for playing sounds
    public AudioClip slapSound; // Sound clip for zombie attack
    public AudioClip explosionSound; // Sound clip for zombie death
    public AudioClip alertSound; // Sound clip for alert state

    [HideInInspector] public NavMeshAgent navMeshAgent; // Reference to the NavMeshAgent component
    private Transform target; // Current target (e.g., player or other agent)
    private float alertTime; // Time when alert state started
    private float attackEndTime; // Time when attack cooldown ends
    private Animator animator; // Reference to the animator component

    void Start()
    {
        audioSource = GetComponent<AudioSource>(); // Get audio source component from the same GameObject

        navMeshAgent = GetComponent<NavMeshAgent>(); // Get NavMeshAgent component
        animator = GetComponent<Animator>(); // Get Animator component
        currentState = ZombieState.Patrol; // Start in patrol state
        navMeshAgent.speed = patrolSpeed; // Set initial speed to patrol speed
        SetRandomDestination(); // Set initial random destination for patrolling
        handCollider.enabled = false; // Disable hand collider initially
        SetAnimatorMode(1); // Set initial animator mode to walk (patrolling)
        SetAnimationSpeed(0.5f); // Set initial animation speed for patrolling
    }

    void Update()
    {
        switch (currentState)
        {
            case ZombieState.Patrol:
                Patrol();
                break;
            case ZombieState.Alerted:
                Alerted();
                break;
            case ZombieState.Pursue:
                Pursue();
                break;
            case ZombieState.Attack:
                Attack();
                break;
            case ZombieState.Death:
                // Do nothing in Update when dead
                break;
        }

        // Check if the death animation has finished
        if (currentState == ZombieState.Death)
        {
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            if (stateInfo.IsName("ZombieDeath") && stateInfo.normalizedTime >= 1.0f)
            {
                // Deactivate the components
                animator.enabled = false;
                navMeshAgent.enabled = false;
                GetComponent<CapsuleCollider>().enabled = false;
            }
        }
    }

    void Patrol()
    {
        // Check if NavMeshAgent is active and has a valid path
        if (navMeshAgent != null && navMeshAgent.isActiveAndEnabled && (!navMeshAgent.hasPath || navMeshAgent.remainingDistance < 1f))
        {
            SetRandomDestination(); // Set new random destination for patrolling
        }

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, detectionRadius); // Check nearby colliders
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag("Player") || hitCollider.CompareTag("CrowdAgent"))
            {
                target = hitCollider.transform; // Set target to player or crowd agent
                currentState = ZombieState.Alerted; // Switch to alerted state
                PlaySoundAtPosition(alertSound); // Play alert sound at zombie position
                alertTime = Time.time + alertDuration; // Set alert end time

                // Reset path only if NavMeshAgent is active and enabled
                if (navMeshAgent.isActiveAndEnabled)
                {
                    navMeshAgent.ResetPath(); // Reset NavMesh path
                }

                SetAnimatorMode(0); // Set animator mode to idle (alerted)
                return;
            }
        }
    }

    void Alerted()
    {
        if (Time.time >= alertTime)
        {
            currentState = ZombieState.Pursue; // Switch to pursue state
            navMeshAgent.speed = pursueSpeed; // Set speed to pursue speed
            SetAnimatorMode(1); // Set animator mode to walk (pursuing)
            SetAnimationSpeed(2.0f); // Set animation speed for pursuing
        }
    }

    void Pursue()
    {
        if (target != null && navMeshAgent != null && navMeshAgent.isActiveAndEnabled)
        {
            // Set destination directly to the target position
            navMeshAgent.SetDestination(target.position);

            // Check if within attack range
            if (Vector3.Distance(transform.position, target.position) <= attackRadius)
            {
                currentState = ZombieState.Attack; // Switch to attack state
                handCollider.enabled = true; // Enable hand collider for attack
                SetAnimatorMode(2); // Set animator mode to attack
                SetAnimationSpeed(1.0f); // Set animation speed for attacking
                PlaySoundAtPosition(slapSound); // Play attack sound at zombie position
            }
        }
        else
        {
            currentState = ZombieState.Patrol; // If no target or NavMeshAgent, return to patrol state
            if (navMeshAgent != null && navMeshAgent.isActiveAndEnabled)
            {
                navMeshAgent.speed = patrolSpeed; // Set speed back to patrol speed
            }
            SetAnimatorMode(1); // Set animator mode to walk (patrolling)
            SetAnimationSpeed(0.5f); // Set animation speed for patrolling
        }
    }

    void Attack()
    {
        if (target != null && Vector3.Distance(transform.position, target.position) <= attackRadius)
        {
            attackEndTime = Time.time + attackCooldown; // Set attack cooldown end time
            currentState = ZombieState.Alerted; // Switch to alerted state
            SetAnimatorMode(0); // Set animator mode to idle during cooldown
        }
        else
        {
            currentState = ZombieState.Pursue; // If target not in range, resume pursuing
            handCollider.enabled = false; // Disable hand collider when not attacking
            SetAnimatorMode(1); // Set animator mode to walk (pursuing)
            SetAnimationSpeed(2.0f); // Set animation speed for pursuing
        }
    }

    public IEnumerator Death()
    {
        SetAnimatorMode(3); // Set animator mode to death
        SetAnimationSpeed(1.0f); // Set animation speed for death
        PlaySoundAtPosition(explosionSound); // Play death sound at zombie position
        yield return new WaitForSeconds(0.5f); // Wait for death animation

        // Find and destroy the MinimapIcon child GameObject
        Transform minimapIconTransform = transform.Find("MinimapIcon");
        if (minimapIconTransform != null)
        {
            Destroy(minimapIconTransform.gameObject);
        }

        // Deactivate the components
        animator.enabled = false;
        navMeshAgent.enabled = false;
        GetComponent<CapsuleCollider>().enabled = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            currentState = ZombieState.Death; // Switch to death state
            StartCoroutine(Death()); // Start death coroutine
        }

        if (collision.gameObject.CompareTag("Car"))
        {
            PlaySoundAtPosition(explosionSound); // Play explosion sound at zombie position
            animator.enabled = false;
            navMeshAgent.enabled = false;
            GetComponent<CapsuleCollider>().enabled = false;
        }
    }

    public void SetRandomDestination()
    {
        Vector3 randomDirection = Random.insideUnitSphere * patrolRadius; // Random direction within patrol radius
        randomDirection += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, patrolRadius, 1); // Sample position on NavMesh
        Vector3 finalPosition = hit.position;
        navMeshAgent.SetDestination(finalPosition); // Set final destination for patrolling
    }

    public void SetAnimatorMode(int mode)
    {
        if (animator != null)
        {
            animator.SetInteger("Mode", mode); // Set animator mode
        }
    }

    public void SetAnimationSpeed(float speed)
    {
        if (animator != null)
        {
            animator.speed = speed; // Set animator speed
        }
    }

    private void PlaySoundAtPosition(AudioClip clip)
    {
        GameObject audioObject = new GameObject("TempAudio");
        audioObject.transform.position = transform.position;
        AudioSource tempAudioSource = audioObject.AddComponent<AudioSource>();
        tempAudioSource.clip = clip;
        tempAudioSource.spatialBlend = 1.0f; // Make the sound 3D
        tempAudioSource.Play();
        Destroy(audioObject, clip.length); // Destroy the audio object after the clip has finished playing
    }
}
