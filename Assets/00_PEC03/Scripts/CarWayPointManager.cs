﻿using UnityEngine;

public class CarWayPointManager : MonoBehaviour
{
    // Base name for waypoints
    public string WpName = "WayPointNameHere";

    void Start()
    {
        // Initialize counter for waypoint numbering
        int counternum = 0;

        // Iterate through each child transform of this game object
        foreach (Transform child in transform)
        {
            // Rename each child game object with the base name and counter number
            child.gameObject.name = WpName + "-" + counternum;
            counternum++;
        }
    }
}
