<h1>PEC3_Programacion3D_2024

<h3>CONTROLS:<h3>

- W/A/S/D to move.
- Mouse movement to look around.
- Left click to shoot.
- Space to jump.
- E to steal the cars.


**[LINK TO PLAY THE GAME](https://drive.google.com/drive/folders/1wniDUSe0r3UPAXrBcG_IB5WtkJHfseIL?usp=sharing)** (The game can be played by double clicking the .exe, no need to install)

**[LINK TO GAMEPLAY](https://youtu.be/wvJOONp4BFY)**

<h2>IMPLEMENTATION SUMMARY:

**Basic points:**

1. The scenario contains a small **square of  a city** with both views to the sea and mountains.
<img src="https://gitlab.com/EduardoJReyes/pec3_programacion3d_2024/-/raw/main/Captures/1.png?ref_type=heads" align="center" height="350" width="600"/>

2. The player can use **an assault rifle** that shoots forward. The player is able to aim to better see where he’s shooting. Whenever he’s aiming, the model will look towards where the aim is directed. 
The **ThirdPersonShooterController.cs** is the one in charge of how the shooting works, instantiating the bullets only when the player is not in the car and not out of bullets.
The **BulletProjectile.cs** will also aid in the shooting by reacting accordingly depending on the target.
<img src="https://gitlab.com/EduardoJReyes/pec3_programacion3d_2024/-/raw/main/Captures/2.png?ref_type=heads" align="center" height="350" width="600"/>

3. The character the player uses has **all of the necessary animations**, all downloaded from Mixamo, using the skeleton and model of the starter assets from Unity. These are walking, idle, aiming and jumping (Start, loop and end). The animation tree is separated in two layers so the upper body will always use the “rifle-grabbing” position.
<img src="https://gitlab.com/EduardoJReyes/pec3_programacion3d_2024/-/raw/main/Captures/3.png?ref_type=heads" align="center" height="350" width="600"/>

4. There is also a **HUD that shows the life and current ammo**. Plus **HP and Ammo items** can be found through the level, restoring the amount of life and ammo the player has.
All of this will be managed by the ThirdPersonShooter.cs as well, depending on the OnTriggerEnter with several tag options and the TakeDamage method.
The items will have a** FloatingObject.cs**, making them float and turn, for aesthetic reasons.

5. **Zombie enemies that patrol, pursue and attack** both the player and bystanders have been implemented. These enemies also have a walking, idle and attacking animation. They can be shot and run over, and whenever this happens they will spawn green blood particles, as well as a mixture of death animation followed by a ragdoll state.

<img src="https://gitlab.com/EduardoJReyes/pec3_programacion3d_2024/-/raw/main/Captures/4_1.png?ref_type=heads" align="center" height="350" width="600"/>

These zombies will use their own walkable navmesh (Mostly everywhere) compared to the humans which will only walk on the side walks and crossings.

<img src="https://gitlab.com/EduardoJReyes/pec3_programacion3d_2024/-/raw/main/Captures/4_3.png?ref_type=heads" align="center" height="350" width="600"/>

When one of their attacks collides with a human, the human is automatically converted into another zombie.
This is managed with the ZombieScript.cs that will let the zombie act on their states patrol, alerted, pursue, attack and death which act on what their names suggest, as well as the Zombiehand.cs on charge of hitting and instantiating more zombies in case they hit a bystander.

<img src="https://gitlab.com/EduardoJReyes/pec3_programacion3d_2024/-/raw/main/Captures/4_2.png?ref_type=heads" align="center" height="350" width="600"/>


6. **Citizens are walking through the city autonomously** and they will attempt to flee when close to a zombie (They are always slower than a zombie sprinting, so they will fail) . These can also die when being run over by a car or shot, automatically converting them into inanimate ragdolls.
These are spawned regularly with the **CitizenSpawner.cs** with specific spawns on a certain time and with a limit of people.
Their actions are governed by the **CitizenState.cs** and their states normal, alerted and knocked, doing what they suggest while their movements are decided in general by the **CrowdManager.cs** moving the citizens through the city.
<img src="https://gitlab.com/EduardoJReyes/pec3_programacion3d_2024/-/raw/main/Captures/78.png?ref_type=heads" align="center" height="350" width="600"/>

7. **Cars are also moving through the streets autonomously** and can run over zombies, citizens and the player as well. These can be “stolen” by the player and used so the player can run over the zombies and entities as well.
The cars move through the level with the **CarWayPointManager.cs **and **CarPatrol.cs**, making them move in an organised way through the waypoints (1,2,3,4, then 1 again)
The car can be used with the CarController.cs, letting the player turn, move forwards and break. 
The ThirdPersonShooterController.cs will be the one deciding when to deactivate the CarPatrol.cs and CarController.cs when the player interacts with the car.
<img src="https://gitlab.com/EduardoJReyes/pec3_programacion3d_2024/-/raw/main/Captures/9.png?ref_type=heads" align="center" height="350" width="600"/>

9. A **starting menu with options** has been implemented, as well as a **game over screen** that lets the player start the game again.
This is controlled with the MenuManager.cs, LevelFade.cs, GameOver.cs, each one of them also fading in and out between the scenes.
<img src="https://gitlab.com/EduardoJReyes/pec3_programacion3d_2024/-/raw/main/Captures/10.png?ref_type=heads" align="center" height="350" width="600"/>

------------------
**Extra points:**
- Every action in the game produces a sound, as seen in the video.
- Players can aim manually to shoot.
- The citizens are transformed into zombies when the zombies successfully attack them.
- When running over citizen or zombies they turn into ragdolls
- A minimap that updates when zombies and humans appear / disappear
- Fade between the scenes
- Player’s jump launches him forward, similar to a superhero jump.

------------------
**FREE RESOURCES USED:** Apart from the free art resources, it is important to note that the base of the movement has been worked with the ThirdPersonCharacter from the starting assets, as a starting point, being modified substantially.

